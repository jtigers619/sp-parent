package org.javatigers.security.oauth2.commons.jwttokens;

import org.javatigers.security.dao.config.AppDBConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.oauth2.provider.token.DefaultAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.DefaultUserAuthenticationConverter;

/**
 * Abstracted configuration for both AuthorizationServer and ResourceServer.
 * 
 * 
 * @author amit.dhiman
 *
 */
@Configuration
@PropertySource(value = "classpath:/META-INF/env/config.properties", ignoreResourceNotFound = true)
@ComponentScan(basePackages = { "org.javatigers.security.oauth2.commons.component", 
		"org.javatigers.security.outh2.commons.service" })
@Import(value = {AppDBConfig.class})
public class AbstractJWTBeansConfig {
	
	/**
	 * Relative path for RSA .jks file
	 */
	protected static final String JKS_FILE_PATH = "jks/jwtaccesstoken.jks";
	
	/**
	 * Relative path for RSA public key file.
	 */
	protected static final String PUBLIC_KEY_FILE_PATH = "jks/public.cert";
	
	/**
	 * User details for authentication.
	 */
	@Autowired
	@Qualifier("appUserDetailsService")
	protected UserDetailsService appUserDetailsService;
	
	/**
	 * RSA key file password.
	 */
	@Value(value = "${jwt.keypass}")
	protected String jksKeyPass;
	
	/**
	 * RSA key file alias.
	 */
	@Value(value = "${jwt.alias}")
	protected String jksAlias;
	
	/**
	 * DefaultAccessTokenConverter configuration. System needs an instance of {@link UserDetailsService} to look for Authenticated user
	 * in {@link DefaultUserAuthenticationConverter#extractAuthentication(java.util.Map)}.
	 * This is a common configuration for both AuthorizationServer and ResourceServer.
	 * 
	 * @return DefaultAccessTokenConverter
	 */
	protected DefaultAccessTokenConverter getDefaultAccessTokenConverter () {
		DefaultUserAuthenticationConverter userTokenConverter = new DefaultUserAuthenticationConverter();
		userTokenConverter.setUserDetailsService(appUserDetailsService);
		DefaultAccessTokenConverter tokenConverter = new DefaultAccessTokenConverter();
		tokenConverter.setUserTokenConverter(userTokenConverter);
		return tokenConverter;
	}
	
}
