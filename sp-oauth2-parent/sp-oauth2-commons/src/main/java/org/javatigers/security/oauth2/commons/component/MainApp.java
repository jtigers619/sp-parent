package org.javatigers.security.oauth2.commons.component;

import java.security.KeyPair;

import org.springframework.core.io.ClassPathResource;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.security.oauth2.provider.token.store.KeyStoreKeyFactory;

public class MainApp {

	public static void main(String[] args) {
		KeyPair keyPair = new KeyStoreKeyFactory(
			    new ClassPathResource("jks/jwtaccesstoken.jks"), "jwtaccesstokenpass".toCharArray())
			    .getKeyPair("jwtaccesstoken", "jwtaccesstokenpass".toCharArray());
		String pubKey = "-----BEGIN PUBLIC KEY-----\n"
				+ new String(Base64.encode(keyPair.getPublic().getEncoded()))
				+ "\n-----END PUBLIC KEY-----";
			System.out.println(pubKey);


	}

}
