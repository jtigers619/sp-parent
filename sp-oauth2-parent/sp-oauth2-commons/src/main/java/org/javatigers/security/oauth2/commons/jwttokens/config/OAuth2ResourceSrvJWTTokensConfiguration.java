package org.javatigers.security.oauth2.commons.jwttokens.config;

import java.io.IOException;
import java.util.Arrays;

import org.apache.commons.io.IOUtils;
import org.javatigers.security.oauth2.commons.component.AppTokenEnhancer;
import org.javatigers.security.oauth2.commons.jwttokens.AbstractJWTBeansConfig;
import org.javatigers.security.outh2.common.constants.ApplicationConstants;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.security.oauth2.provider.token.TokenEnhancerChain;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

/**
 * Common configuration for both OAuth2AthorizationServer and OAuth2ResourceServer. Both Runs on diffrent server 
 * but share the same database and some common components {@link DefaultTokenServices} for token generation and verification.
 * 
 * Its a {@link JwtTokenStore} configuration.
 * 
 * @author amit.dhiman
 *
 */
@Configuration
public class OAuth2ResourceSrvJWTTokensConfiguration extends AbstractJWTBeansConfig {
	
	/** TokenStore implementations bean {@link JdbcTokenStore} stores token.
	 * 
	 * @return TokenStore
	 */
	@Bean
	public TokenStore tokenStore() {
		return new JwtTokenStore(accessTokenConverter());
	}
	
	/**
	 * Note that we used a symmetric key in our JwtAccessTokenConverter to sign our tokens – 
	 * which means we will need to use the same exact key for the Resources Server as well.
	 * 
	 * @return JwtAccessTokenConverter
	 */
	@Bean
	public JwtAccessTokenConverter accessTokenConverter() {
		JwtAccessTokenConverter accessTokenConverter = new JwtAccessTokenConverter();
	    String publicKey = null;
	    try {
	        publicKey = IOUtils.toString(new ClassPathResource(PUBLIC_KEY_FILE_PATH).getInputStream(), ApplicationConstants.UTF_ENCODING);
	    } catch (final IOException e) {
	        throw new RuntimeException(e);
	    }
	    accessTokenConverter.setVerifierKey(publicKey);
	    accessTokenConverter.setAccessTokenConverter(getDefaultAccessTokenConverter ());
		return accessTokenConverter;
	}
	
	/**
	 * OAuth default token service with some extensions.
	 * 
	 * Access token Validity to be 30 minutes
	 * {@link DefaultTokenServices#setAccessTokenValiditySeconds(int)}. Refresh
	 * token Validity to be 8 Hours
	 * {@link DefaultTokenServices#setRefreshTokenValiditySeconds(int)}.
	 * 
	 * Note: this bean is shared b/w Resource server and Authrization server.
	 * 
	 * @param dataSource
	 * @return DefaultTokenServices
	 */
	@Primary
	@Bean(name = "defaultTokenServices")
	public DefaultTokenServices defaultTokenServices() {
		DefaultTokenServices services = new DefaultTokenServices();
		services.setSupportRefreshToken(Boolean.TRUE);
		services.setTokenStore(tokenStore());
		services.setTokenEnhancer(getTokenEnhancerChain ());
		services.setAccessTokenValiditySeconds(1800);
		services.setRefreshTokenValiditySeconds(28800);
		return services;
	}
	
	/**
	 * For token custmization system required JWTTokenConverter which acts as a {@link TokenEnhancer}.
	 * a short of violation of single responsibilty principal.
	 * TODO Split tokenEnhancer from converter.
	 * 
	 * @return TokenEnhancerChain
	 */
	private TokenEnhancerChain getTokenEnhancerChain () {
		TokenEnhancerChain tokenEnhancerChain = new TokenEnhancerChain();
		tokenEnhancerChain.setTokenEnhancers(Arrays.asList(new AppTokenEnhancer(), accessTokenConverter()));
		return tokenEnhancerChain;
	}
}