package org.javatigers.security.oauth2.commons.config;

import javax.sql.DataSource;

import org.javatigers.security.dao.config.AppDBConfig;
import org.javatigers.security.oauth2.commons.component.AppTokenEnhancer;
import org.javatigers.security.oauth2.commons.component.UniqueAuthenticationKeyGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.oauth2.provider.token.AccessTokenConverter;
import org.springframework.security.oauth2.provider.token.DefaultAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;

/**
 * Common configuration for both OAuth2AthorizationServer and OAuth2ResourceServer. Both Runs on diffrent server 
 * but share the same database and some common components {@link DefaultTokenServices} for token generation and verification.
 * 
 * @author amit.dhiman
 *
 */
@Configuration
@PropertySource(value = "classpath:/META-INF/env/config.properties", ignoreResourceNotFound = true)
@ComponentScan(basePackages = { "org.javatigers.security.oauth2.commons.component", 
		"org.javatigers.security.outh2.commons.service" })
@Import(value = {AppDBConfig.class})
public class OAuth2CommonsConfig {
	/**
	 * TokenStore implementations bean {@link JdbcTokenStore} stores token.
	 * 
	 * @param dataSource
	 * @return
	 */
	@Autowired
	@Bean
	public TokenStore tokenStore(DataSource dataSource) {
		JdbcTokenStore jdbcTokenStore = new JdbcTokenStore(dataSource);
		jdbcTokenStore.setAuthenticationKeyGenerator(new UniqueAuthenticationKeyGenerator());
		return jdbcTokenStore;
	}
	
	@Bean
	public AccessTokenConverter accessTokenConverter() {
		return new DefaultAccessTokenConverter();
	}
	
	/**
	 * OAuth default token service with some extensions.
	 * 
	 * Access token Validity to be 30 minutes
	 * {@link DefaultTokenServices#setAccessTokenValiditySeconds(int)}. Refresh
	 * token Validity to be 8 Hours
	 * {@link DefaultTokenServices#setRefreshTokenValiditySeconds(int)}.
	 * 
	 * Note: this bean is shared b/w Resource server and Authrization server.
	 * 
	 * @param dataSource
	 * @return DefaultTokenServices
	 */
	@Autowired
	@Primary
	@Bean(name = "defaultTokenServices")
	public DefaultTokenServices defaultTokenServices(TokenStore tokenStore) {
		DefaultTokenServices services = new DefaultTokenServices();
		services.setSupportRefreshToken(Boolean.TRUE);
		services.setTokenStore(tokenStore);
		services.setTokenEnhancer(new AppTokenEnhancer());
		services.setAccessTokenValiditySeconds(1800);
		services.setRefreshTokenValiditySeconds(28800);
		return services;
	}

}
