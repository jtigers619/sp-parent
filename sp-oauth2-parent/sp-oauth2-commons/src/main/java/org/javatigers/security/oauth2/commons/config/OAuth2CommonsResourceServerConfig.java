package org.javatigers.security.oauth2.commons.config;

import org.javatigers.security.oauth2.commons.component.AppCORSFilter;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.oauth2.provider.token.RemoteTokenServices;

/**
 * A configuration specific to OAuth2ResourceServer which only needs {@link AppCORSFilter} and it does not require database 
 * to be shared becouse it uses the {@link RemoteTokenServices}.
 * 
 * @author amit.dhiman
 *
 */
@Configuration
@PropertySource(value = "classpath:/META-INF/env/config.properties", ignoreResourceNotFound = true)
@ComponentScan(basePackages = { "org.javatigers.security.oauth2.commons.component"})
public class OAuth2CommonsResourceServerConfig {}
