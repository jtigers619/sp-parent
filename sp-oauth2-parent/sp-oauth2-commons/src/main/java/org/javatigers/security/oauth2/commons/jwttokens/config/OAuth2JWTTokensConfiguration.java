package org.javatigers.security.oauth2.commons.jwttokens.config;

import java.util.Arrays;

import org.javatigers.security.dao.config.AppDBConfig;
import org.javatigers.security.oauth2.commons.component.AppTokenEnhancer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenEnhancerChain;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

/**
 * Common configuration for both OAuth2AthorizationServer and OAuth2ResourceServer. Both Runs on diffrent server 
 * but share the same database and some common components {@link DefaultTokenServices} for token generation and verification.
 * 
 * Its a {@link JwtTokenStore} configuration.
 * 
 * @author amit.dhiman
 *
 */
@Configuration
@PropertySource(value = "classpath:/META-INF/env/config.properties", ignoreResourceNotFound = true)
@ComponentScan(basePackages = { "org.javatigers.security.oauth2.commons.component", 
		"org.javatigers.security.outh2.commons.service" })
@Import(value = {AppDBConfig.class})
public class OAuth2JWTTokensConfiguration {
	
	/** TokenStore implementations bean {@link JdbcTokenStore} stores token.
	 * 
	 * @return TokenStore
	 */
	@Bean
	public TokenStore tokenStore() {
		return new JwtTokenStore(accessTokenConverter());
	}
	
	/**
	 * Note that we used a symmetric key in our JwtAccessTokenConverter to sign our tokens – 
	 * which means we will need to use the same exact key for the Resources Server as well.
	 * 
	 * @return JwtAccessTokenConverter
	 */
	@Bean
	public JwtAccessTokenConverter accessTokenConverter() {
		JwtAccessTokenConverter accessTokenConverter = new JwtAccessTokenConverter();
		accessTokenConverter.setSigningKey("123");
		return accessTokenConverter;
	}
	
	/**
	 * OAuth default token service with some extensions.
	 * 
	 * Access token Validity to be 30 minutes
	 * {@link DefaultTokenServices#setAccessTokenValiditySeconds(int)}. Refresh
	 * token Validity to be 8 Hours
	 * {@link DefaultTokenServices#setRefreshTokenValiditySeconds(int)}.
	 * 
	 * Note: this bean is shared b/w Resource server and Authrization server.
	 * 
	 * @param dataSource
	 * @return DefaultTokenServices
	 */
	@Primary
	@Bean(name = "defaultTokenServices")
	public DefaultTokenServices defaultTokenServices() {
		TokenEnhancerChain tokenEnhancerChain = new TokenEnhancerChain();
		tokenEnhancerChain.setTokenEnhancers(Arrays.asList(new AppTokenEnhancer(), accessTokenConverter()));
		DefaultTokenServices services = new DefaultTokenServices();
		services.setSupportRefreshToken(Boolean.TRUE);
		services.setTokenStore(tokenStore());
		services.setTokenEnhancer(tokenEnhancerChain);
		services.setAccessTokenValiditySeconds(1800);
		services.setRefreshTokenValiditySeconds(28800);
		return services;
	}
	
}