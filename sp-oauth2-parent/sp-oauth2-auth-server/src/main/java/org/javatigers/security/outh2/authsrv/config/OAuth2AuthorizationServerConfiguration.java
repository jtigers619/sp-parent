package org.javatigers.security.outh2.authsrv.config;

import org.javatigers.security.oauth2.commons.jwttokens.config.OAuth2AuthSrvJWTTokensConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * OAuth2 component initialization.
 * 
 * 
 * @author amit.dhiman
 *
 */
@Configuration
@EnableWebSecurity/*OAuth2AuthSrvJWTTokensConfiguration.class OAuth2CommonsConfig.classOAuth2JWTTokensConfiguration.class*/
@Import(value = { OAuth2AuthSrvJWTTokensConfiguration.class, AuthorizationServerSecurityConfig.class, OAuth2AuthorizationServer.class})
public class OAuth2AuthorizationServerConfiguration {
	
	/**
	 * User details for authentication.
	 */
	@Autowired
	@Qualifier("appUserDetailsService")
	private UserDetailsService appUserDetailsService;
	
	/**
	 * Global {@link AuthenticationManager} will authenticate users and perform extra check on isActive on users.
	 * 
	 * @param builder
	 * @throws Exception
	 */
	@Autowired
	public void init(AuthenticationManagerBuilder builder) throws Exception {
		builder.userDetailsService(appUserDetailsService).passwordEncoder(new BCryptPasswordEncoder(10));
	}
	
	/**
	 * An object assignable to Spring's BeanFactoryPostProcessor
	 * interface(implementations of BeanFactoryPostProcessor) should be declared
	 * as static if @Bean annotation is present. This will result in a failure
	 * to process annotations such as
	 * 
	 * @Autowired, @Resource and @PostConstruct within the method's
	 * declaring @Configuration class. Add the 'static' modifier to this method
	 * to avoid these container lifecycle issues; see @Bean javadoc for complete
	 * details
	 * 
	 * @return PropertySourcesPlaceholderConfigurer
	 */
	@Bean
	public static PropertySourcesPlaceholderConfigurer propertyPlaceholderConfigurer() {
		return new PropertySourcesPlaceholderConfigurer();
	}
	
	/**
	 * Register {@link MessageSource} get messages from property files.
	 * 
	 * @return {@link MessageSource} - 
	 * 									for internationalization of i18 messages.
	 */
	@Bean
	public MessageSource messageSource() {
		ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
		messageSource.setBasenames("/META-INF/i18n/messages", "/META-INF/i18n/validation-messages");
		messageSource.setUseCodeAsDefaultMessage(Boolean.TRUE);
		return messageSource;
	}

}
