package org.javatigers.security.outh2.authsrv.init;

import java.util.EnumSet;

import javax.servlet.DispatcherType;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

import org.javatigers.security.outh2.authsrv.config.OAuth2AuthorizationServerConfiguration;
import org.javatigers.security.outh2.authsrv.frameworkservlet.config.WebMVCConfiguration;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.filter.DelegatingFilterProxy;
import org.springframework.web.servlet.DispatcherServlet;

/**
 * OAuth2 authorization server intialization.
 * 
 * @author amit.dhiman
 *
 */
public class OAuth2AuthorizationServerIntializer implements WebApplicationInitializer {
	
	private static final String SPRING_SECURITY_FILTER_NAME = "springSecurityFilterChain";
	private static final String DISPATCHER_NAME = "dispatcherServlet";
	private static final Class<?>[] ANNOTATED_CLASSES = { OAuth2AuthorizationServerConfiguration.class };
	
	@Override
	public void onStartup(ServletContext servletContext) throws ServletException {
		this.registerSpringSecurityFilterChain(servletContext);
		this.registerDispatherServlet(servletContext);
		this.registerContextLoaderListener(servletContext);

	}
	
	/**
	 * Register Spring Security Filter chain {@link DelegatingFilterProxy}.
	 * 
	 * @param servletContext
	 */
	private void registerSpringSecurityFilterChain (ServletContext servletContext) {
		servletContext.addFilter(SPRING_SECURITY_FILTER_NAME, DelegatingFilterProxy.class)
	        .addMappingForUrlPatterns(EnumSet.<DispatcherType> of(DispatcherType.REQUEST, DispatcherType.FORWARD, DispatcherType.ASYNC, DispatcherType.INCLUDE), false, "/*");
	}
	
	/**
	 * Register Springs {@link ContextLoaderListener}
	 * 
	 * @param servletContext
	 */
	private void registerContextLoaderListener(ServletContext servletContext) {
		servletContext.addListener(new ContextLoaderListener(createdWebApplicationContext(ANNOTATED_CLASSES)));
	}
	
	/**
	 * Register dispatcher servlet.
	 * 
	 * @param servletContext
	 */
	private void registerDispatherServlet(ServletContext servletContext) {
		ServletRegistration.Dynamic dispatcher = servletContext.addServlet(DISPATCHER_NAME, new DispatcherServlet(
				createdWebApplicationContext(WebMVCConfiguration.class)));
		dispatcher.setLoadOnStartup(1);
		dispatcher.addMapping("/");
	}
	
	/**
	 * Factory method to create {@link WebApplicationContext} instances.
	 * 
	 * @param annotatedClasses
	 * @return
	 */
	private WebApplicationContext createdWebApplicationContext(Class<?>... annotatedClasses) {
		AnnotationConfigWebApplicationContext webApplicationContext = new AnnotationConfigWebApplicationContext();
		webApplicationContext.register(annotatedClasses);
		return webApplicationContext;
	}
}
