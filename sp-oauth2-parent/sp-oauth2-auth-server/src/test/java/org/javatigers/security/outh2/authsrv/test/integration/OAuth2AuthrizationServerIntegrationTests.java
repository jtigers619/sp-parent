package org.javatigers.security.outh2.authsrv.test.integration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.HashMap;
import java.util.Map;

import org.javatigers.security.outh2.authsrv.test.constants.AbstractSpringBasedIntegrationTests;
import org.javatigers.security.outh2.authsrv.test.constants.OAuth2TestConstants;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Value;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;

/**
 * Authorization server integrations tests
 * 
 * @author amit.dhiman
 *
 */
public class OAuth2AuthrizationServerIntegrationTests extends AbstractSpringBasedIntegrationTests {
	
	@Value(value = "${auth.server.base.uri}")
	private String authSrvBaseUri;
	
	@Value(value = "${auth.server.oauth.token.uri}")
	private String authSrvTokenEndpoint;
	
	@Value(value = "${auth.server.oauth.grant_type}")
	private String grantType;
	
	@Value(value = "${auth.server.oauth.client_id}")
	private String clientId;
	
	@Value(value = "${auth.server.oauth.client_secret}")
	private String clientSecret;
	
	@Value(value = "${auth.server.oauth.username}")
	private String username;
	
	@Value(value = "${auth.server.oauth.password}")
	private String password;
	
	private String obtainAccessToken() {
        final Map<String, String> params = new HashMap<String, String>();
        params.put(OAuth2TestConstants.GRANT_TYPE, password);
        params.put(OAuth2TestConstants.CLIENT_ID, clientId);
        params.put(OAuth2TestConstants.USERNAME, username);
        params.put(OAuth2TestConstants.PASSWORD, password);
        final Response response = RestAssured.given().auth().preemptive().basic(clientId, clientSecret).and().with().params(params).when().post(authSrvTokenEndpoint);
        return response.jsonPath().getString(OAuth2TestConstants.ACCESS_TOKEN);
    }

    @Test
    public void givenUserWhenUseCPClientThenOkForMessageResouece() {

        final Response fooResponse = RestAssured.given().header("Authorization", "Bearer " + obtainAccessToken()).get("http://localhost:18080/jaxrs-cxf/api/v1/messages/1");
        assertEquals(200, fooResponse.getStatusCode());
        assertNotNull(fooResponse.jsonPath().get("id"));

        //final Response barResponse = RestAssured.given().header("Authorization", "Bearer " + accessToken).get("http://localhost:8081/spring-security-oauth-resource/bars/1");
        //assertEquals(403, barResponse.getStatusCode());
    }

    /*@Test
    public void givenUser_whenUseBarClient_thenOkForBarResourceReadOnly() {
        final String accessToken = obtainAccessToken("barClientIdPassword", "john", "123");

        final Response fooResponse = RestAssured.given().header("Authorization", "Bearer " + accessToken).get("http://localhost:8081/spring-security-oauth-resource/foos/1");
        assertEquals(403, fooResponse.getStatusCode());

        final Response barReadResponse = RestAssured.given().header("Authorization", "Bearer " + accessToken).get("http://localhost:8081/spring-security-oauth-resource/bars/1");
        assertEquals(200, barReadResponse.getStatusCode());
        assertNotNull(barReadResponse.jsonPath().get("name"));

        final Response barWritResponse = RestAssured.given().contentType(MediaType.APPLICATION_JSON_VALUE).header("Authorization", "Bearer " + accessToken).body("{\"id\":1,\"name\":\"MyBar\"}").post("http://localhost:8081/spring-security-oauth-resource/bars");
        assertEquals(403, barWritResponse.getStatusCode());
    }

    @Test
    public void givenAdmin_whenUseBarClient_thenOkForBarResourceReadWrite() {
        final String accessToken = obtainAccessToken("barClientIdPassword", "tom", "111");

        final Response fooResponse = RestAssured.given().header("Authorization", "Bearer " + accessToken).get("http://localhost:8081/spring-security-oauth-resource/foos/1");
        assertEquals(403, fooResponse.getStatusCode());

        final Response barResponse = RestAssured.given().header("Authorization", "Bearer " + accessToken).get("http://localhost:8081/spring-security-oauth-resource/bars/1");
        assertEquals(200, barResponse.getStatusCode());
        assertNotNull(barResponse.jsonPath().get("name"));

        final Response barWritResponse = RestAssured.given().contentType(MediaType.APPLICATION_JSON_VALUE).header("Authorization", "Bearer " + accessToken).body("{\"id\":1,\"name\":\"MyBar\"}").post("http://localhost:8081/spring-security-oauth-resource/bars");
        assertEquals(201, barWritResponse.getStatusCode());
        assertEquals("MyBar", barWritResponse.jsonPath().get("name"));
    }*/
}
