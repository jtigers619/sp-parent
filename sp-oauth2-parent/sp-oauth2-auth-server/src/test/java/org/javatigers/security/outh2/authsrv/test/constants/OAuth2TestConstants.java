package org.javatigers.security.outh2.authsrv.test.constants;

public interface OAuth2TestConstants {
	
	String CLIENT_ID = "client_id";
	String GRANT_TYPE = "grant_type";
	String USERNAME = "username";
	String PASSWORD = "password";
	String ACCESS_TOKEN = "access_token";
}
