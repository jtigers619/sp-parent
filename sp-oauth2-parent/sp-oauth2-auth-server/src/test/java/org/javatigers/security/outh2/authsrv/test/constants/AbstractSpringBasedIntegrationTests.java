package org.javatigers.security.outh2.authsrv.test.constants;

import org.junit.runner.RunWith;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.test.annotation.IfProfileValue;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Configuration
@PropertySource(value = "classpath:/META-INF/env/config.properties", ignoreResourceNotFound = true)
@RunWith(SpringJUnit4ClassRunner.class)
@IfProfileValue(name = "dao-test", value = "true")
public abstract class AbstractSpringBasedIntegrationTests {
	
	@Bean
	public static PropertySourcesPlaceholderConfigurer propertyPlaceholderConfigurer () {
		return new PropertySourcesPlaceholderConfigurer();
	}
}
