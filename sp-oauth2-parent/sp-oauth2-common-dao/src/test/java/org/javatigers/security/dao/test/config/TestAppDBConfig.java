package org.javatigers.security.dao.test.config;

import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.orm.hibernate5.HibernateExceptionTranslator;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import net.bull.javamelody.SpringDataSourceFactoryBean;

@Configuration
@EnableTransactionManagement
@ComponentScan(value = { "org.javatigers.security.dao" })
public class TestAppDBConfig {
	
	private static final String CLOSE = "close";
	private static final String HIBERNATE_DIALECT = "hibernate.dialect";
	private static final String HIBERNATE_FORMAT_SQL = "hibernate.format_sql";
	private static final String HIBERNATE_HBM2DDL_AUTO = "hibernate.hbm2ddl.auto";
	private static final String HIBERNATE_EJB_NAMING_STRATEGY = "hibernate.ejb.naming_strategy";
	private static final String HIBERNATE_SHOW_SQL = "hibernate.show_sql";
	private static final String HIBERNATE_SHOW_GENERATED_DDL= "hibernate.show.generated.ddl";
	private static final String SECURITY_MODELS_PERSISTENCE_UNIT_NAME = "spsecurity-model";
	
	/**
	 * Boolean value to decide whether initialize DB with seed values or not.
	 */
	@Value(value = "${initialize.database:false}")
	private String initializeDatabase;
	
	/**
	 * Database driver for connection.
	 */
	@Value (value = "${database.driver}")
	private String databaseDriver;
	
	/**
	 * Database URL for connection.
	 */
	@Value (value = "${database.url}")
	private String databaseURL;
	
	/**
	 * Database username for connection.
	 */
	@Value (value = "${database.username}")
	private String databaseUsername;
	
	/**
	 * Database password for connection.
	 */
	@Value (value = "${database.password}")
	private String databasePassword;
	
	/**
	 * JPA property describes that whether to print JPA query on console or not. If this property is true logs will be printed on console.
	 */
	@Value (value = "${hibernate.show.sql}")
	private String hibernateShowSQL;
	
	/**
	 * Environment has access to all environmental variable and properties
	 * values.
	 */
	@Autowired
	private Environment environment;
	
	/**
	 * Data source for connection with database.
	 * 
	 * @return {@link DataSource} - for DB connection.
	 */
	@Bean(destroyMethod=CLOSE)
	public DataSource targetDataSource () {
		BasicDataSource dataSource = new BasicDataSource();
		dataSource.setDriverClassName(databaseDriver);
		dataSource.setUrl(databaseURL);
		dataSource.setUsername(databaseUsername);
		dataSource.setPassword(databasePassword);
		return dataSource;
	}
	
	@Bean
	public SpringDataSourceFactoryBean dataSource () {
		SpringDataSourceFactoryBean factoryBean = new SpringDataSourceFactoryBean();
		factoryBean.setTargetName("targetDataSource");
		return factoryBean;
	}
	
	/**
	 * JPA adapter for hibernate.
	 * 
	 * @return {@link HibernateJpaVendorAdapter} - return vendor adapter for JPA.
	 */
	private HibernateJpaVendorAdapter getHibernateJpaVendorAdaptor () {
		
		HibernateJpaVendorAdapter adapter = new HibernateJpaVendorAdapter();
		adapter.setGenerateDdl(Boolean.parseBoolean(environment.getProperty(HIBERNATE_SHOW_GENERATED_DDL)));
		adapter.setShowSql(Boolean.parseBoolean(environment.getProperty(HIBERNATE_SHOW_SQL)));
		adapter.setDatabasePlatform(environment.getProperty(HIBERNATE_DIALECT));
		adapter.setDatabase(Database.MYSQL);
		return adapter;
	}
	
	/**
	 * Use dataSource and hibernateJpaVendorAdapter to create entityManagerFactory.
	 * 
	 * @return {@link LocalContainerEntityManagerFactoryBean} - cunstructed entityManagerFactory.
	 */
	@Bean
	public LocalContainerEntityManagerFactoryBean localContainerEntityManagerFactoryBean () {
		LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
		entityManagerFactoryBean.setDataSource((DataSource) dataSource ());
		entityManagerFactoryBean.setJpaVendorAdapter(getHibernateJpaVendorAdaptor ());
		//TODO below setPackagesToScan is not required. Its here for reference.
		//entityManagerFactoryBean.setPackagesToScan(JPA_ENTITIES);
		entityManagerFactoryBean.setJpaProperties(jpaProperties());
		entityManagerFactoryBean.afterPropertiesSet();
		
		return entityManagerFactoryBean;
	}
	
	/**
	 * Create and return {@link JpaTransactionManager} for transactions.
	 * 
	 * @param entityManagerFactoryBean
	 * @return {@link JpaTransactionManager} - transaction manager to use while database oprations.
	 */
	@Autowired
	@Bean
	public JpaTransactionManager transactionManager(LocalContainerEntityManagerFactoryBean entityManagerFactoryBean) {
		JpaTransactionManager jpaTransactionManager = new JpaTransactionManager(entityManagerFactoryBean.getObject());
		jpaTransactionManager.setPersistenceUnitName(SECURITY_MODELS_PERSISTENCE_UNIT_NAME);
		jpaTransactionManager.setValidateExistingTransaction(Boolean.TRUE);
		jpaTransactionManager.setDefaultTimeout(TransactionDefinition.TIMEOUT_DEFAULT);
		return jpaTransactionManager;
	}
	
	/**
	 * Use {@link HibernateExceptionTranslator} for exceptions.
	 * 
	 * @return {@link HibernateExceptionTranslator}.
	 */
	@Bean
	public HibernateExceptionTranslator hibernateExceptionTranslator() {
		return new HibernateExceptionTranslator();
	}
	
	/**
	 * Hibernate properties to use.
	 * 
	 * @return {@link Properties} - to use.
	 */
	private Properties jpaProperties() {
		Properties props = new Properties();
		props.setProperty(HIBERNATE_DIALECT, environment.getProperty(HIBERNATE_DIALECT));
		props.setProperty(HIBERNATE_FORMAT_SQL, environment.getProperty(HIBERNATE_FORMAT_SQL));
		props.setProperty(HIBERNATE_HBM2DDL_AUTO, environment.getProperty(HIBERNATE_HBM2DDL_AUTO));
		props.setProperty(HIBERNATE_EJB_NAMING_STRATEGY, environment.getProperty(HIBERNATE_EJB_NAMING_STRATEGY));
		props.setProperty(HIBERNATE_SHOW_SQL, environment.getProperty(HIBERNATE_SHOW_SQL));
		// add more
		return props;
	}
	
}
