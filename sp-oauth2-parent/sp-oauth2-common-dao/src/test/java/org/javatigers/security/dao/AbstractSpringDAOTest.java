package org.javatigers.security.dao;

import java.sql.Connection;
import java.util.List;

import javax.inject.Inject;
import javax.sql.DataSource;

import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.ext.mysql.MySqlDataTypeFactory;
import org.dbunit.operation.DatabaseOperation;
import org.javatigers.security.dao.test.config.TestCommonDAOConfig;
import org.junit.Assert;
import org.junit.runner.RunWith;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.test.annotation.IfProfileValue;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Base test execution configuration for spring based DAO test classes.
 * 
 * @author amit.dhiman
 *
 */
@ContextConfiguration(classes = { TestCommonDAOConfig.class })
@RunWith(SpringJUnit4ClassRunner.class)
@IfProfileValue(name = "dao-test", value = "true")
public abstract class AbstractSpringDAOTest {
	
	private static final String DATA_SET_BASE_LOC = "dbunit"; 
	
	@Inject
    private DataSource dataSource;

    protected abstract String getExportFileName();
    
    protected Connection getDBConnection() {
    	return DataSourceUtils.getConnection(dataSource);
    }

    /**
     * Method to get the Data set.
     * 
     * @return IDataSet.
     * @throws Exception
     *             exception.
     */
    private IDataSet getDataSet() throws Exception {
        IDataSet dataSet = new FlatXmlDataSetBuilder().build( new ClassPathResource(DATA_SET_BASE_LOC + "/" + getExportFileName()).getInputStream());
        return dataSet;
    }

    /**
     * Method to get the Data connection.
     * 
     * @return IDatabaseConnection.
     * @throws Exception
     *             exception.
     */
    private IDatabaseConnection getConnection() throws Exception {
        // get connection
        IDatabaseConnection connection = new DatabaseConnection(getDBConnection());
        DatabaseConfig config = connection.getConfig();
        config.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new MySqlDataTypeFactory());
        return connection;
    }

    protected void setUp() throws Exception {
        DatabaseOperation.CLEAN_INSERT.execute(getConnection(), getDataSet());
        DataSourceUtils.releaseConnection(getDBConnection(), dataSource);
    }
    
    protected void tearDown() throws Exception {
        DatabaseOperation.DELETE_ALL.execute(getConnection(), getDataSet());
        DataSourceUtils.releaseConnection(getDBConnection(), dataSource);
    }

    /**
     * Custom assertion to test any list operation in Service tests.
     * 
     * @param expectedSize
     * @param actualList
     */
    protected void assertList(int expectedSize, List<?> actualList) {
        Assert.assertNotNull(actualList);
        Assert.assertEquals(expectedSize, actualList.size());
    }
}
