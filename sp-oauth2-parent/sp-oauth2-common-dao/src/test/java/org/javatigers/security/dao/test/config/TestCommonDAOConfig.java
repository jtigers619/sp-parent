package org.javatigers.security.dao.test.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

@Configuration
@PropertySource(value = { "classpath:/META-INF/db/jdbc-hibernate.properties" }, ignoreResourceNotFound = true)
@Import(value = {TestAppDBConfig.class})
public class TestCommonDAOConfig {

	@Bean
	public static PropertySourcesPlaceholderConfigurer propertyPlaceholderConfigurer () {
		return new PropertySourcesPlaceholderConfigurer();
	}
	
}
