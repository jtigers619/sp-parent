package org.javatigers.security.dao.test.users;

import javax.inject.Inject;
import javax.persistence.NoResultException;

import org.javatigers.security.dao.AbstractSpringDAOTest;
import org.javatigers.security.dao.config.UserDAOConfig;
import org.javatigers.security.dao.users.UsersDAO;
import org.javatigers.security.model.users.Users;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.Assert;

import org.springframework.test.context.ContextConfiguration;

/**
 * Test cases for {@link UserDAO}
 * 
 * @author amit.dhiman
 *
 */
@ContextConfiguration(classes = { UserDAOConfig.class })
public class UsersDAOTest extends AbstractSpringDAOTest {

	private static final String USERS_DB_UNIT_FILE_PATH = "users-export.xml";
	
	private static final String VALID_USERNAME = "cpapi";
	private static final String VALID_EMAIL = "amit.dhiman@trantorinc.com";
	private static final String CONTACT_ID = "N/A";
	private static final String INVALID_USERNAME = "N/A";
	private static final boolean IS_ACTIVE = true;
	
	private static final String NO_RESULT_EXCEPTION = "No entity found for query";
	
	@Inject
	private UsersDAO userDAO;
	
	@Override
	protected String getExportFileName() {
		return USERS_DB_UNIT_FILE_PATH;
	}
	
	/**
     * Method to set the Mock data.
     * 
     * @throws java.lang.Exception
     */
	@Before
	@Override
	public void setUp () throws Exception {
		super.setUp();
	}
	
	 /**
     * Method to delete the data base content.
     * 
     * @throws java.lang.Exception
     */
    @Override
    @After
    public void tearDown() throws Exception {
        super.tearDown();
    }
    
    /**
     * Test read {@link Users} entity with {@link Users#getUsername()} with valid username.
     */
    @Test
    public void testReadUserWithUsername () {
    	Users actualUser = this.userDAO.readUser(VALID_USERNAME);
    	assertUsers(getUser (), actualUser);
    }
    
    /**
     * Test read {@link Users} entity with {@link Users#getUsername()} with invalid username.
     */
    @Test(expected = NoResultException.class)
    public void testReadUserWithInvalidUsername () {
    	try {
    		this.userDAO.readUser(INVALID_USERNAME);
		} catch (NoResultException noResultException) {
			Assert.assertEquals(NO_RESULT_EXCEPTION, noResultException.getMessage());
			throw noResultException;
		}
    	
    }
    
    /**
     * {@link Users} entity assertion.
     * 
     * @param actualUser
     * @param expectedUser
     */
    private void assertUsers (Users expectedUser, Users actualUser) {
    	Assert.assertNotNull(actualUser);
    	Assert.assertEquals(expectedUser.getUsername(), actualUser.getUsername());
    	Assert.assertEquals(expectedUser.getEmail(), actualUser.getEmail());
    	Assert.assertEquals(expectedUser.getContactId(), actualUser.getContactId());
    	Assert.assertEquals(expectedUser.getIsActive(), actualUser.getIsActive());
    }
    
    /**
     * Prepare {@link Users} entity.
     * 
     * @return {@link Users} entity.
     */
    private Users getUser () {
    	Users user = new Users();
    	user.setUsername(VALID_USERNAME);
    	user.setEmail(VALID_EMAIL);
    	user.setContactId(CONTACT_ID);
    	user.setIsActive(IS_ACTIVE);
    	return user;
    }
}
