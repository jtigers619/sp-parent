package org.javatigers.security.dao;

import java.io.FileOutputStream;

import org.dbunit.database.IDatabaseConnection;
import org.dbunit.database.QueryDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;

public class TestDataGeneration {
	
	public void generateExportXML (IDatabaseConnection iDatabaseConnection) throws Exception {
        QueryDataSet partialDataSet = new QueryDataSet(iDatabaseConnection);
     // oracle schema name is the user name
        //String schemaOwner = new JdbcTemplate(dataSource).queryForObject("select sys_context( 'userenv', 'current_schema' ) from dual", String.class);
        // Mention all the tables here for which you want data to be extracted
        // take note of the order to prevent FK constraint violation when re-inserting
        //table which has foreign key defined in it appears immediate below of table who's primary key is defined as
        // foreign key in another table. eg. In many to many relation both table primery keys defined as foreign keys
        // in third table in that case third dependent table apears at the end.
        partialDataSet.addTable("app_users");
        partialDataSet.addTable("roles");
        partialDataSet.addTable("app_users_roles");
        // XML file into which data needs to be extracted
        FlatXmlDataSet.write(partialDataSet, new FileOutputStream("D:\\users-export.xml"));

    }
}
