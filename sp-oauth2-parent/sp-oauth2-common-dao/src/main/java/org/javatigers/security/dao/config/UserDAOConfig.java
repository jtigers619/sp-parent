package org.javatigers.security.dao.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "org.javatigers.security.dao.users")
public class UserDAOConfig {

}
