package org.javatigers.security.outh2.resourcesrv.jwtconfig;

import org.javatigers.security.oauth2.commons.jwttokens.config.OAuth2ResourceSrvJWTTokensConfiguration;
import org.javatigers.security.outh2.resourcesrv.config.MethodSecurityConfig;
import org.javatigers.security.outh2.resourcesrv.config.OAuth2ResourceServerDefaultToken;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, /*securedEnabled = true, */proxyTargetClass = true)
@Import(value = {/*OAuth2JWTTokensConfiguration.class*/OAuth2ResourceSrvJWTTokensConfiguration.class, OAuth2ResourceServerDefaultToken.class, MethodSecurityConfig.class})
@Order(Ordered.HIGHEST_PRECEDENCE)
public class OAuth2ResourceServerJWTTokenIntializer {
	
}
