package org.javatigers.security.outh2.resourcesrv.config;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.javatigers.security.oauth2.commons.component.AppCORSFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.authentication.BearerTokenExtractor;
import org.springframework.security.oauth2.provider.authentication.TokenExtractor;
import org.springframework.security.oauth2.provider.error.OAuth2AccessDeniedHandler;
import org.springframework.security.oauth2.provider.error.OAuth2AuthenticationEntryPoint;
import org.springframework.security.oauth2.provider.expression.OAuth2WebSecurityExpressionHandler;
import org.springframework.security.oauth2.provider.token.AccessTokenConverter;
import org.springframework.security.oauth2.provider.token.DefaultAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.RemoteTokenServices;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.access.channel.ChannelProcessingFilter;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;
import org.springframework.web.filter.OncePerRequestFilter;

/**
 * Resource server protect api resources.
 * 
 * @author amit.dhiman
 *
 */
@Configuration
@EnableResourceServer
public class OAuth2ResourceServer extends ResourceServerConfigurerAdapter {
	
	/**
	 * Resource id.
	 */
	private static final String RESOURCE_ID = "com.lendingpoint.cp";
	
	private TokenExtractor tokenExtractor = new BearerTokenExtractor();
	
	/**
	 * CORS filter required origin to pass through.
	 */
	@Autowired
	private AppCORSFilter appCORSFilter;
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void configure(ResourceServerSecurityConfigurer resources)
			throws Exception {
		resources.resourceId(RESOURCE_ID);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void configure(HttpSecurity http) throws Exception {
		// @formatter:off	
			http
				.addFilterBefore(appCORSFilter, ChannelProcessingFilter.class)
				.addFilterAfter(new OncePerRequestFilter() {
					@Override
					protected void doFilterInternal(HttpServletRequest request,
							HttpServletResponse response, FilterChain filterChain)
							throws ServletException, IOException {
						// We don't want to allow access to a resource with no token so clear
						// the security context in case it is actually an OAuth2Authentication
						if (tokenExtractor.extract(request) == null) {
							SecurityContextHolder.clearContext();
						}
						filterChain.doFilter(request, response);
					}
				}, AbstractPreAuthenticatedProcessingFilter.class)
				.authorizeRequests()
				.expressionHandler(new OAuth2WebSecurityExpressionHandler())
				.antMatchers("/api/messages").hasRole("CREATE_USER")
				.antMatchers("/api/v1/*").hasRole("API")
				.antMatchers("/index","/swagger-ui/**").permitAll()
				
			.and()
				.anonymous().disable()
				.csrf().disable()
				.exceptionHandling()
					.authenticationEntryPoint(oauthAuthenticationEntryPoint())
					.accessDeniedHandler(new OAuth2AccessDeniedHandler());
			// @formatter:on
	}
	
	/**
	 * AuthenticationEntryPoint bean declarations.
	 * 
	 * @return AuthenticationEntryPoint
	 */
	@Bean
	protected AuthenticationEntryPoint oauthAuthenticationEntryPoint() {
		OAuth2AuthenticationEntryPoint entryPoint = new OAuth2AuthenticationEntryPoint();
		entryPoint.setRealmName("Authorization");
		return entryPoint;
	}
	
	@Bean
	public AccessTokenConverter accessTokenConverter() {
		return new DefaultAccessTokenConverter();
	}
	
	@Bean
	public RemoteTokenServices remoteTokenServices(final @Value("${auth.server.url}") String checkTokenUrl,
			final @Value("${auth.server.clientId}") String clientId,
			final @Value("${auth.server.clientsecret}") String clientSecret) {
		final RemoteTokenServices remoteTokenServices = new RemoteTokenServices();
		remoteTokenServices.setCheckTokenEndpointUrl(checkTokenUrl);
		remoteTokenServices.setClientId(clientId);
		remoteTokenServices.setClientSecret(clientSecret);
		remoteTokenServices.setAccessTokenConverter(accessTokenConverter());
		return remoteTokenServices;
	}
}

