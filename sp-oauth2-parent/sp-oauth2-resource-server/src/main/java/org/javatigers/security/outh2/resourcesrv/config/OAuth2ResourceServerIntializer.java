package org.javatigers.security.outh2.resourcesrv.config;

import org.javatigers.security.oauth2.commons.config.OAuth2CommonsResourceServerConfig;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, /*securedEnabled = true, */proxyTargetClass = true)
@Import(value = {OAuth2CommonsResourceServerConfig.class, OAuth2ResourceServer.class, MethodSecurityConfig.class})
@Order(Ordered.HIGHEST_PRECEDENCE)
public class OAuth2ResourceServerIntializer {
	
}
