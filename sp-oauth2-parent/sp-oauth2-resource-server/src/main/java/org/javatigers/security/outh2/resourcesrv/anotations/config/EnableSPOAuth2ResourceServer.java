package org.javatigers.security.outh2.resourcesrv.anotations.config;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.javatigers.security.outh2.resourcesrv.config.OAuth2ResourceServerIntializer;
import org.springframework.context.annotation.Import;

/**
 * Enable the OAuth2Resource server on resources.
 * 
 * @author amit.dhiman
 *
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import(value = {OAuth2ResourceServerIntializer.class})
public @interface EnableSPOAuth2ResourceServer {

}
